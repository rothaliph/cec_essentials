var es_leng = "";

if(cec_essentials==undefined)
{
    var cec_essentials = "http://sistemas.cecltda.cl/cec/ditorium_desarrollo/cec_essentials/";
}
if(api_general==undefined)
{
    var api_general = "http://sistemas.cecltda.cl/cec/ditorium_desarrollo/api_desarrollo/";
}

    var sw_desarrollo = true;

function aConsola(string)
{
    if(sw_desarrollo)
    {
        console.log(string);
    }

}

function isEmpty(obj)
{
    if (typeof obj == 'undefined' || obj === null || obj === '') return true;
    if (typeof obj == 'number' && isNaN(obj)) return true;
    if (obj instanceof Date && isNaN(Number(obj))) return true;
    return false;
}

function getJSON(url,data)
{
    var data = data || {};

    var arr;
    $.ajax(
    {
        type:"POST",
        async: false,  
        cache: false,      
        url: url,        
        dataType: 'json',
        data: data ,
        success: function(datos)
        {
            arr = datos;
        }
    })
    .fail(function() 
    {
        cerrar_dialog_message();
        abrir_dialog_message("Ocurrió un error durante la carga de uno o mas datos.<br>Porfavor recarge la página y reintente","error");
        arr = {};
    });
    return arr;
}


function contenidoPrincipal(pagina,datos)
{   
    var datos = datos || null;  
    cargarContenidoID("main-container",pagina,datos);
}

function contenidoSubPrincipal(pagina,datos)
{	
	var datos = datos || null;	
	cargarContenidoID("sub-container",pagina,datos);
}

function validar_select2_global(field, rules, i, options)
{   
    if(isEmpty(field.select2("val")))
    {
        // console.log("vacio");
        rules.push("required");
        return true;
    }
    return false;
}

function val_rut_global(field, rules, i, options)
{
    var rut = field.val();
    var bol = $.Rut.validar(rut);

    if(!bol)
    {
        return "* Rut erroneo";
    }
}

function cargarContenidoID(id,pagina,datos) 
{
	// console.log(id + " <-> "+pagina);
	// $("#"+id).html(ajax_load);
    mostrarLoading();  

	var datos = datos || null;

	if(!isEmpty(datos))
	{
		$.ajax(
		{
	        async: false,  
	        cache: false,
	        type:"POST",      
	        url: pagina,
	        data: datos ,
	        success: function(datos)
	        {
                cerrar_dialog_message();
                $("#"+id).html(datos);              
            }
        }).fail(function() 
        {
            cerrar_dialog_message();
            abrir_dialog_message("Ocurrió un error durante la carga de la página. Es posible que la página a la que intenta acceder no exista.<br>Porfavor reintente","error");
        });
    }
    else
    {
        $.ajax(
        {
            async: false,  
            cache: false,               
            url: pagina,            
            success: function(datos)
            {
                cerrar_dialog_message();
	        	$("#"+id).html(datos);	            
	        }
	    }).fail(function() 
        {
            cerrar_dialog_message();
            abrir_dialog_message("Ocurrió un error durante la carga de la página. Es posible que la página a la que intenta acceder no exista.<br>Porfavor reintente","error");
        });
	}
}

function mostrarLoading()
{
    abrir_dialog_message("Espere unos momentos...",'loading');
	
}

function cerrar_dialog_message()
{
    $("#error_message_dialog").dialog('close')
}

function cerrarLoading()
{
    $("#error_message_dialog").dialog('close')
}


function iniciar_datatable_basica(iddatatable, apiData, funcionPersonalizada,info,esconderBuscador,paging,columnDef)
{
    var columnDef = columnDef || {};
    var funcionPersonalizada = funcionPersonalizada || fn_vacia;
    var info = info || false;
    var paging = paging || false;
    var esconderBuscador = esconderBuscador || false;

    return $('#'+iddatatable).DataTable( 
    {
    	"paging": paging,
		"bProcessing": true,
		"bServerSide": true,
        "bFilter": !esconderBuscador,
    	"info": info,
		"sAjaxSource": apiData,
        "fnServerData": function ( sSource, aoData, fnCallback ) {
            $.getJSON( sSource, aoData, function (json) 
            { 
            	// console.log(status);
                var key, count = 0;
                for(key in json["aaData"]) 
                {
                  if(json["aaData"].hasOwnProperty(key)) 
                  {
                    count++;
                  }
                }
                for(var i = 0; i<count ; i++)
                {
                    json["aaData"][i] = funcionPersonalizada(json["aaData"][i]);
                }
                fnCallback(json);
            } ).fail(function(data){
                abrir_dialog_message("Ocurrió un error mientras se cargaban los datos. <br>Porfavor Reintente.","error");
                $('#'+iddatatable).DataTable().clear();
                $('#'+iddatatable+"_processing").hide();

            });
        },
        "columnDefs" : columnDef,
        "oLanguage": es_leng,
        "order" : [[0,"DESC"]],
    });
}

function agregarFilaATabla(tabla, fila)
{
    var l = $('table#'+tabla+' tbody tr').length;
    if(l>0)
    {
        $('table#'+tabla+' tr:last').after(fila);    
    }
    else
    {
        $('table#'+tabla+' tbody').append(fila);
    } 
}


function iniciar_datatable_basica_vacia(iddatatable,info)
{
    var info = info || false;

    return $('#'+iddatatable).DataTable( 
    {
    	"paging": info,
    	"searching": info,
        "bSort": false,
    	"info": info,
        "oLanguage": es_leng,
        // "order" : [[0,"DESC"]],
    });
}

function iniciar_dialog_dinamico()
{
    $("#dialog_vacio").dialog({
        modal: true,
        buttons: {"CERRAR": function(){$(this).dialog("close");}},
        autoOpen: false,
        closeOnEscape: true,
        minWidth: 950,
        resizable: false,
        maxHeight:600,
    });
}

function iniciar_dialog_message()
{
    $("#error_message_dialog").dialog({
        modal: true,
        buttons: {"Aceptar": function(){$(this).dialog("close");}},
        autoOpen: false,
        closeOnEscape: true,
        minWidth: 400,
        resizable: false,
        maxHeight:400,
        // minHeight:300,
        dialogClass:"dialog_sin_header",
    });
}

function iniciar_dialog_creacion()
{
    $("#dialog_creacion").dialog({
        modal: true,
        buttons: {"Aceptar": function(){$(this).dialog("close");}, "Cerrar": function(){$(this).dialog("close");}},
        autoOpen: false,
        closeOnEscape: true,
        minWidth: 400,
        resizable: false,
        maxHeight:400,
        // minHeight:300,
        dialogClass:"dialog_sin_header",
    });
}

function cerrar_dialog_dinamico()
{
    $("#dialog_vacio").dialog('close');
}
function abrir_dialog_dinamico(titulo)
{
    var titulo = titulo || "";
    $("#dialog_vacio").dialog({ title: titulo });
    $("#dialog_vacio").dialog('open');
}

function abrir_dialog_creacion()
{
    $("#dialog_creacion").dialog('open');
}

// abrir_dialog_message('Este es un mensaje de prueba','error',[{text:"Aceptar", click:function(){$("#error_message_dialog").dialog('close');}},{text:"Cancelar", click:function(){$("#error_message_dialog").dialog('close');}}])
function abrir_dialog_message(mensaje,icono,buttons)
{
    if(buttons===undefined)
    {
        var buttons = [{text:"Aceptar", click:function(){$("#error_message_dialog").dialog('close');}}];
    }
    $("#error_message_dialog").dialog({buttons:buttons});
    var icon = '';
    $("#error_message_text").html(mensaje);
    $("#error_message_dialog").dialog({closeOnEscape: true});
    switch(icono)
    {
        case "error":
            icon = '<i style="color:#981a1a;" class="fas fa-exclamation-circle" aria-hidden="true"></i>';
            break;
        case "alerta":
            icon = '<i style="color:#e49d16;" class="fas fa-exclamation-triangle" aria-hidden="true"></i>';
            break;
        case "aceptar":
            icon = '<i style="color:#009659;" class="far fa-check-square" aria-hidden="true"></i>';
            break;
        case "loading":
            $("#error_message_dialog").dialog({closeOnEscape: false});
            icon = '<i style="color:#1323e5;" class="fas fa-cog fa-spin" aria-hidden="true"></i>';
            break;
        case "clock":
            icon = '<i class="far fa-clock" aria-hidden="true"></i>';
            break;
        case "none":
            icon = '';
            break;
        default:
            icon = '<i style="color:#981a1a;" class="fas fa-exclamation-circle" aria-hidden="true"></i>';
            break;  
    }
    $("#error_message_icon").html(icon);

    $("#error_message_dialog").dialog('open')
}

function funcion_vacia()
{
    return undefined;
}


function a_entero(value)
{
    if(isEmpty(value)){return 0;}
    if(!$.isNumeric(value))
    {
        return 0;;
    }
    else
    {
        return parseInt(value);
    }
}

function a_numerico(value)
{
    if(isEmpty(value)){return 0;}
    if(!$.isNumeric(value))
    {
        return 0;;
    }
    else
    {
        return parseFloat(value);
    }
}

function app_init_plugins()
{

    es_leng = getJSON(cec_essentials+"plugins/DataTables/dataTables.Spanish.json");

    iniciar_dialog_dinamico();
    iniciar_dialog_creacion();
    iniciar_dialog_message();

    numeral.locale('es');
    numeral.defaultFormat('$0,0.00');

    // $.fn.dataTable.ext.errMode = function ( settings, helpPage, message )
    // { 
    //     console.log(message);
    //     abrir_dialog_message("Ocurrió un error mientras se cargaban los datos. <br>Porfavor Reintente.","error");
    // };

}

function a_precio(num)
{
    return numeral(Number(num)).format();
}

function precio_a_numero(val)
{
    var str = String(val);
    var res = "";
    for (var i = 0; i < str.length; i++) 
    {
        if(str[i]!="$" && str[i]!=".")
        {
            if(str[i]==",")
            {
                res = res + ".";
            }
            else
            {
                res = res + str[i];
            }
        }
    }
    console.log(res);
    return a_numerico(res);
}

$(document).on("focus",".input_precio",function()
{
    var num = $(this).val();
    num = precio_a_numero(num);
    $(this).val(num);
    $(this).select();
    
});

$(document).on("blur",".input_precio",function()
{
    var num = $(this).val();
    if(num.match(/^\$?(?=\(,*\)|[^()]*$)\(?\d{1,3}(.?\d{3})?(\,\d\d?)?\)?$/))
    {
        num = ""+a_numerico(num);
        // return true;
    }
    num = num.replace("$",'');
    num = a_numerico(num);
    $(this).val(a_precio(num));

});